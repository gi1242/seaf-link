# seaf-link: Manage symlinks in Seafile

## Synopsis

This is a script to manage symbolic links in folders that are synchronized by
[Seafile]. Currently (2019-05-13), seafile blindly follows symlinks. If there
is a recursive loop, Seafile will follow the loop until you are out of space
or dragons scorch the earth.

This script removes all symlinks and creates placeholder files (which are
synced by seafile). Then it adds all the symlink names to `seafile-ignore.txt`
so they are not synced by Seafile. Finally it re-creates the symlinks from the
placeholder files.

## Usage

Just run `seaf-link /path/to/your/library`. It might take up to three runs to
synchronize.

Here's what the script does:

1. For each link `foo`, it creates a placeholder file (called
   `.foo.seaf-link`) containing the target of the symlink. Then it removes
   `foo`.

2. If any symlinks were deleted, the script exits and asks you to re-run after
   Seafile synchronizes. (This is so that that these symlinks get deleted on
   the server and all clients.)

3. Next, it creates a section in `seafile-ignore.txt` containing names of all
   symlinks corresponding to placeholder files. (I.e. for `.foo.seaf-link`, it
   will add `foo`.)

4. If any lines were added / removed to `seafile-ignore.txt`, the script
   exits and asks you to re-run it after Seafile synchronizes. This is so that
   when the symlinks are re-created (in the next step), Seafile won't attempt
   to ~~mis~~manage them.

5. For every placeholder file, recreate the symlink if possible. If a file /
   link already exists (and has a different target) it WILL NOT be
   overwritten. The script prints a conflict message. These need to be
   manually resolved (by deleting the file or placeholder file), and then the
   script needs to be re-run.

## Managing symlinks:

1. Creating new symlinks: You can create the link, then run `seaf-link` the
   right number of times. This will start synchronizing your symlink. If you
   don't want that, then here's an alternate way:

        echo 'target' > .foo.seaf-link
        seaf-link
        # Wait for seafile to synchronize
        seaf-link
        # Wait for seafile to synchronize

2. Deleting existing symlinks. Deleting a link will cause it to be re-created
   on subsequent runs. You should also delete the placeholder file. For
   instance, to delete the symlink `foo` do:

        rm -f .foo.seaf-link
        rm -f foo
        touch foo   # Sends your empty copy of foo to others, instead of
                    # downloading a possibly big symlink.
        seaf-link
        # Wait for seafile to synchronize
        rm -f foo   # Deletes foo for you and all clients

3. Renaming symlinks: To rename the symlink `foo` to `bar` do:

        mv .foo.seaf-link .bar.seaf-link
        rm -f foo
        touch foo
        seaf-link
        # Wait for seafile to synchronize
        seaf-link
        # Wait for seafile to synchronize
        rm -f foo

4. Changing the target of symlinks:

        echo 'new-target' > .foo.seaf-link
        rm -f foo
        seaf-link

## Credits

This script is based on a similar bash script
[seafile-symlink](https://github.com/IllyaMoskvin/seafile-symlink). I rewrote
it in perl, and made sure it pauses for seafile to resync at the right times.
There is also an open Seafile support request
[here](https://github.com/haiwen/seafile/issues/288). If you're interested in
seeing symlinks properly implemented in Seafile itself, please comment /
upvote the bug.

[Seafile]: https://www.seafile.com/en/home/
